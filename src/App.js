import React, {Component} from 'react';
import classes from './App.module.css';
import Form from './containers/Form/Form';
import { BrowserRouter } from 'react-router-dom';


class App extends Component {
    render() {
        return (
           <BrowserRouter>
               <div className={classes.App}>
                   <Form />
               </div>
           </BrowserRouter>
        );
    }
}

export default App;
