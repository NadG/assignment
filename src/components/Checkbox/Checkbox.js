import React from 'react';
import classes from './Checkbox.module.css'

const checkbox = props => {
    let checkboxList = props.options.map((e, i) => {
        return <label
                        key={i}>
                        <input
                            value={e}
                            type="checkbox"
                            onChange={props.change}
                        />
                        {e}
                    </label>
    })
    return (
        <div className={classes.Checkbox}>
            {checkboxList}
        </div>
    )
}

export default checkbox
