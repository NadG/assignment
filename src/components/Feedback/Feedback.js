import React from 'react';
import classes from './Feedback.module.css'

const feedback = props => {
    return (
            <h1 className={classes.Gradiented}>
               {props.message}
            </h1>
    )
}

export default feedback
