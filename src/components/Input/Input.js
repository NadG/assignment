import React from 'react';
import classes from './Input.module.css'

const input = props => {
    return (
        <div className={classes.Input}>
            <input
                value={props.value}
                type="text"
                onChange={props.clicked}
            />
        </div>
    )
}

export default input
