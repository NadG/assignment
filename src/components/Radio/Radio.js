import React from 'react';
import classes from './Radio.module.css'
import Aux from '../../hoc/auxiliary'

const input = props => {
    let radioGroup = props.options.map((e, i) => {
        return (
                <label
                    key={i}>
                    <input
                        value={e}
                        type="radio"
                        name="group"
                        onChange={props.change}
                    />
                    {e}
                </label>
        )
    })

    return (
        <div className={classes.Radio}>
            {radioGroup}
        </div>
    )
}

export default input
