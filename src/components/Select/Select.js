import React from 'react';
import classes from './Select.module.css'

const select = props => {
    let option = props.options.map( (e, i) => {
        return <option value={e} key={i} >{e}</option>
    })

    return (
        <select value={props.stateVal} className={classes.Select} onChange={props.changeOp}>
            {option}
        </select>
    )
}

export default select
