// input di testo, una select, un gruppo di 3 radio e un gruppo di 3 checkbox.
import React, {Component} from 'react';
import classes from './Form.module.css'
import Input from '../../components/Input/Input';
import Checkbox from '../../components/Checkbox/Checkbox';
import Radio from '../../components/Radio/Radio';
import Select from '../../components/Select/Select';
import Button from '../../components/ui/Button/Button';
import Feedback from '../../components/Feedback/Feedback';
import Aux from '../../hoc/auxiliary'
import axios from 'axios'

class Form extends Component {
    state = {
        data: {
            specialization: ['Front end', 'Back end', 'Full stack'],
            skills: ['Js', 'React', 'Angular', 'Css'],
            age: ['18-25', '26-30', '30+']
        },
        userInput: {
            name: '',
            specialization: '',
            skills: [],
            age: ''
        },
        showButton: false,
        currentStep: 1
    }

    changeOp = (e) => {
        this.setState({
            userInput: {
                ...this.state.userInput,
                specialization: e.target.value
            },
            showButton: true
        })
        e.preventDefault();
    }

    inputChange = (e) => {

        this.setState({
            userInput: {
                ...this.state.userInput,
                name: e.target.value
            },

            showButton: true
        })
        e.preventDefault();
    }

    radioChange = (e) => {
        this.setState({
            userInput: {
                ...this.state.userInput,
                age: e.target.value,
            },
            showButton: true
        })
    }

    skillsSelection = (e) => {
        let skillsSelected = this.state.userInput.skills
        let check = e.target.checked;
        let checkedSkill = e.target.value;
        if (check) {
            this.setState({
                userInput: {
                    ...this.state.userInput,
                    skills: [...this.state.userInput.skills, checkedSkill]
                },
            })
        } else {
            let index = skillsSelected.indexOf(checkedSkill);
            if (index > -1) {
                skillsSelected.splice(index, 1);
                this.setState({
                    userInput: {
                        ...this.state.userInput,
                        skills: skillsSelected
                    }
                })
            }
        }
        this.state.userInput.skills.length > 0 ? this.setState({showButton: true}) : this.setState({showButton: false})

    }

    renderCurrentStep = () => {
        switch (this.state.currentStep) {
            case 1 :
                return (
                    <Aux>
                        <section>
                            <h4 className={classes.Labels}>Name</h4>
                            <Input value={this.state.name} label="Name" clicked={this.inputChange}/>
                        </section>
                        <section>
                            <h4 className={classes.Labels}>Age range</h4>
                            <Radio options={this.state.data.age} change={this.radioChange}/>
                        </section>
                    </Aux>
                )
            case 2:
                return (
                    <section style={{width: '200px'}}>
                        <h4 className={classes.Labels}>Specialization</h4>
                        <Select
                            options={this.state.data.specialization}
                            changeOp={this.changeOp}
                            stateVal={this.state.userInput.specialization}/>
                    </section>
                )
            case 3:
                return (
                    <section>
                        <h4 className={classes.Labels}>Skills</h4>
                        <Checkbox
                            options={this.state.data.skills}
                            change={this.skillsSelection}/>
                    </section>
                )
            case 4:
                return (
                    <section>
                        <Feedback message="Thank you!"/>
                    </section>
                )
            default :
                return 1
        }
    }

    saveData = () => {
        const data = {
            name: this.state.userInput.name,
            specialization: this.state.userInput.specialization,
            skills: this.state.userInput.skills.join(' '),
            age: this.state.userInput.age,
        };
        axios.post('/saved.json', data)
            .then(response => {
                this.setState({
                    showFeedback: true,
                    showButton: false
                })
            })
            .catch(err => console.log(err));
        setTimeout(() => {
            this.setState({
                currentStep: this.state.currentStep + 1,
                showFeedback: false
            })
        }, 1200)
    }

    render() {
        let buttonToRender = this.state.currentStep < 4 && this.state.showButton ?
            <Button label="Save" clicked={this.saveData}/> : null
        let feedback = this.state.showFeedback ? <Feedback message="Saved!"/> : null
        return (
            <div className={classes.Form}>
                <section style={{margin: 0}}>
                    <h3>Form</h3>
                </section>
                <section>
                    {this.renderCurrentStep()}
                </section>
                <section style={{height: '115px'}}>
                    {feedback}
                    {buttonToRender}
                </section>
                <section>
                    <p>coded with 💛 by <a className={classes.Link} target="_blank" href="https://gitlab.com/NadG?nav_source=navbar">Nadia Guarracino</a></p>
                </section>
            </div>
        )
    }
}

export default Form
